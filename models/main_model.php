<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetStaffProfile($perid)
    {
        $sql = 'SELECT NAME, SURNAME FROM medperson WHERE PERID = ' . $perid . '';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }
}
