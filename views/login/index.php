<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ระบบจองโต๊ะ</title>
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="views/login/index.css">
  <link rel="stylesheet" href="bower_components/bootstrap-sweetalert/dist/sweetalert.css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
  <style>
    html,
    body {
      margin: 0;
      padding: 0;
      height: 100%;
    }

    .background {
      background-image: url("public/src/img/dinner.jpg");
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size: 100% 100%;
    }

    @font-face {
      font-family: niramit;
      src: url(public/fonts/niramit/TH-Niramit-AS.ttf);
    }

    @font-face {
      font-family: niramit-bold;
      src: url(public/fonts/niramit/TH-Niramit-AS-Bold.ttf);
    }

    div {
      font-family: niramit;
    }
  </style>
</head>

<body class="background">
  <div class="wrapper fadeInDown">
    <!-- <a href="https://medhr.medicine.psu.ac.th/SingleSign/medmail" id="login">MED Login</a> -->
    <img class="mb-3" src="public/src/img/ER_Building.jpg" width="150" height="150" alt="" style="border-radius: 50%;">
    <div id="formContent">
      <form>
        <div class="row">
          <div class="col fadeIn first">
            <i class="fa fa-envelope" style="font-size: 18px;"></i>
            <input type="text" id="email" class="form-control input_pass" placeholder="อีเมล" value="famz@test.com" autocomplete="off" style="font-size:22px;">
          </div>

        </div>
        <div class="row">
          <div class="col fadeIn second">
            <i class="fa fa-lock" style="font-size: 24px;"></i>
            <input type="password" id="password" class="form-control input_pass" placeholder="รหัสผ่าน" value="password" autocomplete="off" style="font-size:22px;">
          </div>
        </div>

        <div class="row">
          <div class="col fadeIn third">
            <input type="button" value="Log In" onclick="Login()" style="font-size:22px;">
          </div>
        </div>
      </form>
    </div>
  </div>

  <script src="bower_components/tether/dist/js/tether.min.js"></script>
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="bower_components/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
  <!-- <script src="https://medhr.medicine.psu.ac.th/SingleSign/accessService?js"></script> -->
  <script>
    // $.ajax({
    //     type:"GET",
    //     url: "https://medhr.medicine.psu.ac.th/SingleSign/accessService",
    //     header:{
    //       'Content-Type': 'application/x-www-form-urlencoded'
    //     },
    //     // data: JSON.stringify(logindata),
    //     contentType: "application/json",
    //     dataType: "json",
    //     success: function(response){
    //       console.log(response);
    //     },
    //     error: function(e){
    //       console.log(e);
    //     }
    //   });

    // $.get("https://medhr.medicine.psu.ac.th/SingleSign/accessService", function(json){
    //   if (json.status) {
    //     console.log("Login success");
    //   } else{
    //     console.log("Login failed!");
    //     $("#login").attr("href", SSMedmail);
    //   }
    // });

    function Login(){
      var logindata = {"email": $('#email').val() ,
                      "password": $('#password').val()};
      // console.log(logindata);
      $.ajax({
        type:"POST",
        url: "../tablereservation/ApiService/Login",
        header:{
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: JSON.stringify(logindata),
        contentType: "application/json",
        dataType: "json",
        success: function(response){
          console.log(response);
          if (response.length != 0) {
            // response.forEach(element => {
            //   console.log(element['NAME']);
            // });

            swal({
                title: "เข้าสู่ระบบสำเร็จ",
                // text: window.location.href,
                showConfirmButton: false,
                type: "success",
                timer: 1500,
            }, function() {
                window.location.href = "../tablereservation/main";
            });
          }
        },
        error: function(e){
          console.log(e);
        }
      });

    }
  </script>

</body>

</html>
