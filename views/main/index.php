<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ระบบจองโต๊ะ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="colorlib.com">
    <link rel="stylesheet" href="bower_components\bootstrap\dist\css\bootstrap.min.css">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <!-- <link rel="stylesheet" href="public/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- STYLE CSS -->
    <!-- <link rel="stylesheet" href="views/main/index.css"> -->
    <link rel="stylesheet" href="public/css/freelancer.css">
    <link rel="stylesheet" href="bower_components/bootstrap-sweetalert/dist/sweetalert.css">

    <style>
        html {
            width: auto;
            height: 100vh;
        }

        body {
            font-family: "Lato-Bold";
            background-image: url("public/src/img/form-wizard-bg.jpg");
            background-attachment: fixed;
            background-position: fill;
            background-size: cover;
        }
    </style>
</head>

<body>
    <div class="text-center" style="margin-top: 16px; margin-bottom: 16px;">
        <img src="public/src/img/booking-step-1-active.png" alt="" class="step-arrow" id="step1">
        <img src="public/src/img/arrow.png" alt="" class="step-arrow" id="arrow1">
        <img src="public/src/img/booking-step-2.png" alt="" class="step-arrow" id="step2">
        <img src="public/src/img/arrow.png" alt="" class="step-arrow" id="arrow2">
        <img src="public/src/img/booking-step-3.png" alt="" class="step-arrow" id="step3">
    </div>

    <section id="section1" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mx-auto">
                    <form>
                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people1" name="people1"
                                    placeholder="รหัสบุคลากร 1" value="46773" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people1')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>

                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people2" name="people2"
                                    placeholder="รหัสบุคลากร 2" value="46774" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people2')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people3" name="people3"
                                    placeholder="รหัสบุคลากร 3" value="12345" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people3')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people4" name="people4"
                                    placeholder="รหัสบุคลากร 4" value="11111" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people4')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people5" name="people5"
                                    placeholder="รหัสบุคลากร 5" value="22222" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people5')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people6" name="people6"
                                    placeholder="รหัสบุคลากร 6" value="55555" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people6')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people7" name="people7"
                                    placeholder="รหัสบุคลากร 7" value="46875" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people7')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people8" name="people8"
                                    placeholder="รหัสบุคลากร 8" value="95863" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people8')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people9" name="people9"
                                    placeholder="รหัสบุคลากร 9" value="23568" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people9')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-10">
                                <input type="text" class="form-control" id="people10" name="people10"
                                    placeholder="รหัสบุคลากร 10" value="78523" maxlength="5">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn" onclick="ShowProfile('people10')"
                                    style="width:100%; color:white; background-color: #8eb852;">
                                    <i class="fa fa-user" style="font-size: 18px;"></i>
                                </button>
                            </div>
                        </div>

                        <input type="button" class="btn" value="ต่อไป" onclick="regist()"
                            style="width:100%; color:white; background-color: #8eb852;" />
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="section2" class="page-section">
        <div class="container text-center">
            <h2 style="color: black;">หมายเลขโต๊ะ</h2>
            <h2 id="tableNumber" style="color: black;">000</h2>
            <!-- <h3 style="color: black;">เลือกโต๊ะ</h3> -->
            <ol>
                <li class="row row--1">
                    <p class="mr-3">A</p>
                    <ol id="table-row1" class="seats" type="A">
                    </ol>
                </li>

                <li class="row row--2">
                    <p class="mr-3">B</p>
                    <ol id="table-row2" class="seats" type="B">
                    </ol>
                </li>

                <li class="row row--3">
                    <p class="mr-3">C</p>
                    <ol id="table-row3" class="seats" type="C">
                    </ol>
                </li>

                <li class="row row--4">
                    <p class="mr-3">D</p>
                    <ol id="table-row4" class="seats" type="D">
                    </ol>
                </li>

                <li class="row row--5">
                    <p style="margin-right: 18px;">E</p>
                    <ol id="table-row5" class="seats" type="E">
                    </ol>
                </li>

                <li class="row row--6 mt-5">
                    <p class="mr-3">F</p>
                    <ol id="table-row6" class="seats" type="F">
                    </ol>
                </li>

                <li class="row row--7">
                    <p class="mr-3">G</p>
                    <ol id="table-row7" class="seats" type="G">
                    </ol>
                </li>

                <li class="row row--8">
                    <p class="mr-3">H</p>
                    <ol id="table-row8" class="seats" type="H">
                    </ol>
                </li>

                <li class="row row--9">
                    <p style="margin-right: 22px;">I</p>
                    <ol id="table-row9" class="seats" type="I">
                    </ol>
                </li>

                <li class="row row--10 mt-5">
                    <p style="margin-right: -2px;">J</p>
                    <ol id="table-row10" class="seats" type="J">
                    </ol>
                </li>

                <li class="row row--11" style="margin-top: -10px;">
                    <p style="margin-right: -4px;">K</p>
                    <ol id="table-row11" class="seats" type="K">
                    </ol>
                </li>

                <li class="row row--12 mt-2">
                    <p style="margin-right: -2px;">L</p>
                    <ol id="table-row12" class="seats" type="L">
                    </ol>
                </li>

                <li class="row row--13" style="margin-top: -10px;">
                    <p style="margin-right: -7px;">M</p>
                    <ol id="table-row13" class="seats" type="M">
                    </ol>
                </li>

                <li class="row row--14 mt-2">
                    <p style="margin-right: -4px;">N</p>
                    <ol id="table-row14" class="seats" type="N">
                    </ol>
                </li>

                <li class="row row--15" style="margin-top: -10px;">
                    <p style="margin-right: -4px;">O</p>
                    <ol id="table-row15" class="seats" type="O">
                    </ol>
                </li>
            </ol>
            <!-- <div align="center">
                    <img id="table" class="mt-5" src="public/src/img/table.jpg" alt="">
                </div> -->
            <button class="btn" onclick="goToStep1()" style="color:white; background-color: #8eb852;">ย้อนกลับ</button>
            <button class="btn" id="selectedBtn" onclick="selectedTable()" disabled
                style="color:white; background-color: #d3d3d3; cursor: not-allowed;">ยืนยัน</button>
        </div>

    </section>

    <section id="section3" class="page-section">
        <div class="container text-center">
            <h3>ข้อมูลการจองโต๊ะ</h3>
            <div class="row" style="align-items: center; justify-content: center;">
                <div class="col-auto text-center">
                    <h3>ชื่อ-สกุล:</h3>
                </div>
                <div class="col-auto text-center">
                    <h3 id="reservedName"></h3>
                </div>
            </div>

            <div class="row" style="align-items: center; justify-content: center;">
                <div class="col-auto text-center">
                    <h3>หมายเลขโต๊ะ:</h3>
                </div>
                <div class="col-auto text-center">
                    <h3 id="tableNumberFinal"></h3>
                </div>
            </div>
            <h3>สมาชิก</h3>
            <h3>1</h3>
            <h3>2</h3>
            <h3>3</h3>
            <h3>4</h3>
            <h3>5</h3>
            <h3>6</h3>
            <h3>7</h3>
            <h3>8</h3>
            <h3>9</h3>
            <h3>10</h3>

            <button class="btn" onclick="goToStep2()" style="color:white; background-color: #8eb852;">ย้อนกลับ</button>
            <button class="btn" id="confirmBtn" onclick="confirm()"
                style="color:white; background-color: #8eb852;">ยืนยัน</button>
        </div>
    </section>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script>
        //Constructor Function
        var count = 1;
        var tableselected = "";
        $(function () {
            $("#section2").hide();
            $("#section3").hide();
        });

        function tableRowAtoB(rownum, rowlabel) {
            for (let index = 1; index <= 35; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 1);
                switch (true) {
                    case index < 23:
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + index + "' onclick='getTable(this)'/>" +
                            "<label for='" + rowlabel + index + "'>" + index + "</label>" +
                            "</li>";
                        break;
                    case index == 23:
                        tableRow = " <li class='seat'>X</li>";
                        break;
                    case (index >= 24 && index <= 27):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    case index == 28:
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='reservemuslim' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    case (index >= 29 && index <= 34):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='muslim' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    default:
                        break;
                }
                $("#table-row" + rownum).append(tableRow);
            }
        }

        function tableRowC() {
            for (let index = 1; index <= 35; index++) {
                var tableRow = "";
                switch (true) {
                    case (index >= 1 && index <= 27):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='C" + index + "' onclick='getTable(this)'/>" +
                            "<label for='C" + index + "'>" + index + "</label>" +
                            "</li>";
                        break;
                    case index == 28:
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='C" + index + "' onclick='getTable(this)'/>" +
                            "<label class='reservemuslim' for='C" + index + "'>" + index + "</label>" +
                            "</li>";
                        break;
                    case (index >= 29 && index <= 34):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='C" + index + "' onclick='getTable(this)'/>" +
                            "<label class='muslim' for='C" + index + "'>" + index + "</label>" +
                            "</li>";
                        break;
                    default:
                        break;
                }
                $("#table-row3").append(tableRow);
            }
        }

        function tableRowDtoE(rownum, rowlabel) {
            for (let index = 1; index <= 35; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 2);
                switch (true) {
                    case (index >= 1 && index <= 17):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + index + "' onclick='getTable(this)'/>" +
                            "<label for='" + rowlabel + index + "'>" + index + "</label>" +
                            "</li>";
                        break;
                    case (index == 18 || index == 19):
                        tableRow = " <li class='seat'>X</li>";
                        break;
                    case (index >= 20 && index <= 27):

                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    case index == 28:
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='reservemuslim' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    case (index >= 29 && index <= 34):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='muslim' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    default:
                        break;
                }
                $("#table-row" + rownum).append(tableRow);
            }
        }

        function tableRowE() {
            for (let index = 1; index <= 35; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 2);
                switch (true) {
                    case (index >= 1 && index <= 17):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='E" + index + "' onclick='getTable(this)'/>" +
                            "<label for='E" + index + "'>" + index + "</label>" +
                            "</li>";
                        break;
                    case (index == 18 || index == 19):
                        tableRow = " <li class='seat'></li>";
                        break;
                    case (index >= 20 && index <= 27):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='E" + curIndex + "' onclick='getTable(this)'/>" +
                            "<label for='E" + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    case index == 28:
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='E" + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='reservemuslim' for='E" + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    case (index >= 29 && index <= 34):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='E" + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='muslim' for='E" + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    default:
                        break;
                }
                $("#table-row5").append(tableRow);
            }
        }

        function tableRowFtoI(rownum, rowlabel) {
            for (let index = 1; index <= 35; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 10);
                switch (true) {
                    case (index >= 1 && index <= 10):
                        tableRow = " <li class='seat'></li>";
                        break;
                    case (index >= 11 && index <= 31):
                        tableRow = " <li class='seat'>" +
                            "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    default:
                        break;
                }
                $("#table-row" + rownum).append(tableRow);
            }
        }

        function tableRowJtoK(rownum, rowlabel) {
            for (let index = 1; index <= 35; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 12);
                switch (true) {
                    case (index >= 1 && index <= 12):
                        tableRow = " <li class='seat'></li>";
                        break;
                    case (index >= 13 && index <= 31):
                        if (index % 2 != 0) {
                            tableRow = " <li class='seat'>" +
                                "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        } else {
                            tableRow = " <li class='seat mr-2'>" +
                                "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        }
                        break;
                    case (index == 32):
                        tableRow = " <li class='seat'>" +
                            "<input disabled type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                            "<label class='reserveplayer' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                            "</li>";
                        break;
                    default:
                        break;
                }
                $("#table-row" + rownum).append(tableRow);
            }
        }

        function tableRowLtoM(rownum, rowlabel) {
            for (let index = 1; index <= 37; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 12);
                switch (true) {
                    case (index >= 1 && index <= 12):
                        tableRow = " <li class='seat'></li>";
                        break;
                    case (index >= 13 && index <= 31):
                        if (index % 2 != 0) {
                            tableRow = " <li class='seat'>" +
                                "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        } else {
                            tableRow = " <li class='seat mr-2'>" +
                                "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        }
                        break;
                    case (index >= 32 && index <= 34):
                        if (index % 2 != 0) {
                            tableRow = " <li class='seat'>" +
                                "<input disabled type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label class='reserveplayer' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        } else {
                            tableRow = " <li class='seat mr-2'>" +
                                "<input disabled type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label class='reserveplayer' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        }

                        break;
                    default:
                        break;
                }
                $("#table-row" + rownum).append(tableRow);
            }
        }

        function tableRowNtoO(rownum, rowlabel) {
            for (let index = 1; index <= 32; index++) {
                var tableRow = "";
                curIndex = parseInt(index - 24);
                switch (true) {
                    case (index >= 1 && index <= 12):
                        tableRow = " <li class='seat'></li>";
                        break;

                    case (index >= 13 && index <= 24):
                        if (index % 2 != 0) {
                            tableRow = " <li class='seat'></li>";
                        } else {
                            tableRow = " <li class='seat mr-2'></li>";
                        }
                        break;

                    case (index >= 25 && index <= 32):
                        if (index % 2 != 0) {
                            tableRow = " <li class='seat'>" +
                                "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label class='reservebuddhist' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        } else {
                            tableRow = " <li class='seat mr-2'>" +
                                "<input type='checkbox' id='" + rowlabel + curIndex + "' onclick='getTable(this)'/>" +
                                "<label class='reservebuddhist' for='" + rowlabel + curIndex + "'>" + curIndex + "</label>" +
                                "</li>";
                        }
                        break;
                    default:
                        break;
                }
                $("#table-row" + rownum).append(tableRow);
            }
        }

        function goToStep1() {
            // document.getElementById("step2").src = "public/src/img/step-2-active.png";
            $("input").removeAttr('disabled');
            $("#step1").attr("src", "public/src/img/booking-step-1-active.png");
            $("#step2").attr("src", "public/src/img/booking-step-2.png");
            $("#step3").attr("src", "public/src/img/booking-step-3.png");

            $("#section1").fadeIn(900);
            $("#section2").fadeOut(900);
            $("#section2").hide();
            $("#section3").fadeOut(900);
            $("#section3").hide();

            $("#back").hide();
            $("#next").show();

            $("#selectedBtn").attr("style", "background-color: #d3d3d3; cursor: not-allowed;");
            $("#selectedBtn").attr("disabled", "disabled");
            $("#tableNumber").text("000");
            if (tableselected != "") {
                $("#" + tableselected)[0].checked = false;
            }
            count = 1;
            tableselected = "";
        }

        function goToStep2() {

            // document.getElementById("step2").src = "public/src/img/step-2-active.png";
            // $("#selectedBtn").attr("style", "background-color: #d3d3d3; cursor: not-allowed;");
            // $("#selectedBtn").attr("disabled", "disabled");
            // $("#tableNumber").text(tableselected);
            // count = 1;
            $("#step1").attr("src", "public/src/img/booking-step-1-active.png");
            $("#step2").attr("src", "public/src/img/booking-step-2-active.png");
            $("#step3").attr("src", "public/src/img/booking-step-3.png");

            $("#section1").fadeOut(900);
            $("#section1").hide();
            $("#section2").fadeIn(900);
            $("#section3").fadeOut(900);
            $("#section3").hide();

            $("#back").show();
            $("#next").show();

            $("#table-row1").html("");
            $("#table-row2").html("");
            $("#table-row3").html("");
            $("#table-row4").html("");
            $("#table-row5").html("");
            $("#table-row6").html("");
            $("#table-row7").html("");
            $("#table-row8").html("");
            $("#table-row9").html("");
            $("#table-row10").html("");
            $("#table-row11").html("");
            $("#table-row12").html("");
            $("#table-row13").html("");
            $("#table-row14").html("");
            $("#table-row15").html("");

            tableRowAtoB("1", "A");
            tableRowAtoB("2", "B");
            tableRowC();
            tableRowDtoE("4", "D");
            tableRowDtoE("5", "E");
            tableRowFtoI("6", "F");
            tableRowFtoI("7", "G");
            tableRowFtoI("8", "H");
            tableRowFtoI("9", "I");
            tableRowJtoK("10", "J");
            tableRowJtoK("11", "K");
            tableRowLtoM("12", "L");
            tableRowLtoM("13", "M");
            tableRowNtoO("14", "N");
            tableRowNtoO("15", "O");

            if (tableselected != "") {
                $("#" + tableselected)[0].checked = true;
                $("input").attr("disabled", "disabled");
                $("#" + tableselected).removeAttr('disabled');
                $("#selectedBtn").attr("style", "color:white; background-color: #8eb852; cursor: pointer;");
                $("#selectedBtn").removeAttr('disabled');
            }
        }

        function goToStep3() {
            $("#reservedName").text("ทดสอบ เสมอมา");
            $("#tableNumberFinal").text(tableselected);
            $("#step1").attr("src", "public/src/img/booking-step-1-active.png");
            $("#step2").attr("src", "public/src/img/booking-step-2-active.png");
            $("#step3").attr("src", "public/src/img/booking-step-3-active.png");

            $("#section1").fadeOut(900);
            $("#section1").hide();
            $("#section2").fadeOut(900);
            $("#section2").hide();
            $("#section3").fadeIn(900);

            $("#next").hide();
        }

        function ShowProfile(people) {
            // console.log($("#" + people)[0].value);
            var medperid = {
                "perid": $('#' + people).val()
            };
            // console.log(medperid);
            $.ajax({
                type: "POST",
                url: "../tablereservation/ApiService/GetStaffProfile",
                header: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(medperid),
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    console.log(response);

                    swal({
                        title: response[0].NAME + " " + response[0].SURNAME,
                        // text: 'Modal with a custom image.',
                        imageUrl: 'https://unsplash.it/400/200',
                        imageWidth: 800,
                        imageHeight: 800,
                        imageAlt: 'Custom image',
                    })
                },
                error: function () {
                    console.log('Error occured');
                }
            });


            // swal({
            //     title: 'Sweet!',
            //     text: 'Modal with a custom image.',
            //     imageUrl: 'https://unsplash.it/400/200',
            //     imageWidth: 400,
            //     imageHeight: 200,
            //     imageAlt: 'Custom image',
            // })
        }
        function regist() {
            // console.log($('#section1').css('display'));
            // console.log($('#section2').css('display'));
            // if ($('#section1').css('display') === "block") {
            //     goToStep2();
            // } else if($('#section2').css('display') === "block"){
            //     goToStep3()
            // }
            var peridList = $("form").serializeArray();
            var next = true;
            peridList.forEach(element => {
                console.log(element);
                if (element.value == "") {
                    next = false;
                }
            });

            if (next) {
                goToStep2();
            } else {
                swal({
                    title: "กรุณากรอกสมาชิกให้ครบ",
                    // text: window.location.href,
                    showConfirmButton: true,
                    confirmButtonColor: '#d33',
                    confirmButtonText: "ตกลง",
                    type: "warning",
                });
            }

        }

        function selectedTable() {
            goToStep3();
        }

        function getTable(table) {
            tableselected = table.id;
            console.log(table.id);
            $("#tableNumber").text(table.id);
            // $("input").attr("disabled", "disabled");
            // $("#"+table.id).removeAttr('disabled');

            if (count % 2 == 0) {
                $("input").removeAttr('disabled');
                $("#tableNumber").text("000");
                $("#selectedBtn").attr("style", "background-color: #d3d3d3; cursor: not-allowed;");
                $("#selectedBtn").attr("disabled", "disabled");
            } else {
                $("input").attr("disabled", "disabled");
                $("#" + table.id).removeAttr('disabled');
                $("#selectedBtn").attr("style", "color:white; background-color: #8eb852; cursor: pointer;");
                $("#selectedBtn").removeAttr('disabled');
            }
            // var ol = document.getElementsByTagName("ol");
            // var li = ol[1].getElementsByTagName("li");
            // var input = li[0].getElementsByTagName("input");
            // // console.log(input[0].id);
            // for (var i = 0; i < ol.length; i++) {
            //     var li = ol[i].getElementsByTagName("li");
            //     console.log(li[i]);
            // //     console.log(inputs[i].getElementsByTagName("input"));
            // // //     if (inputs[i].getElementsByTagName("input")[0].id != table.id) {
            // // //         inputs[i].disabled = true;
            // // //     } else{
            // // //         inputs[i].disabled = false;
            // // //     }
            // }
            // console.log(count);
            count += 1;
        }

    </script>

    <!-- <script src="views/main/js/jquery-3.3.1.min.js"></script> -->

    <!-- JQUERY STEP -->
    <!-- <script src="views/main/js/jquery.steps.js"></script>

    <script src="views/main/js/main.js"></script> -->

    <!-- Template created and distributed by Colorlib -->
</body>

</html>