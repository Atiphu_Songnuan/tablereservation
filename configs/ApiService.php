<?php
class ApiService
{
    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    //******** Login ********//
    public function Login()
    {
        $this->loadModel("login");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->Login($request->email, $request->password);
    }

    //List All exefileData
    public function GetStaffProfile()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GetStaffProfile($request->perid);
    }
}
