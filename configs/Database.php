<?php
class Database extends PDO
{
    public function __construct()
    {
        $db_type = "mysql";
        $db_host = "localhost:8888";
        $db_name = "tablereservation;charset=utf8";
        $db_user = "root";
        $db_pass = "";

        try {
            parent::__construct($db_type . ':host=' . $db_host . ';dbname=' . $db_name, $db_user, $db_pass);
            // echo "Connect database successfully!";
        } catch (PDOException $e) {
            echo "Error: " . $e;
        }
        // header('Content-Type: application/json');
        // // Create connection
        // $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        // $this->conn->set_charset("utf8");
    }
}
