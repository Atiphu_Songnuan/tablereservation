<?php
class View
{
    public function render($name)
    {
        require_once 'views/' . $name . '.php';
    }
}
